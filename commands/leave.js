module.exports = {
    name: 'leave',
    description: 'leaves the current voice channel',
    execute(message) {
        message.guild.me.voiceChannel.leave();
    },
};
