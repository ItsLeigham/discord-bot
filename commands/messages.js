module.exports = {
    name: 'messages',
    description: 'messages',
    execute(message, args) {
        message.channel
            .fetchMessages({
                limit: 10,
            }).then(messages => {
                const user = message.mentions.users.first();
                const filterBy = user ? user.id : message.guild.me.user.id;
                messages = messages
                    .filter(m => m.author.id === filterBy)
                    .array()
                    .slice(0, 10);

                console.log(user.id);
                console.log(messages[0].author.id);

                messages[0].delete();
            }).catch(console.error);
    },
};
