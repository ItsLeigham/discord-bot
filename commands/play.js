/* eslint-disable no-await-in-loop */
const ytdl = require('ytdl-core');
const YouTube = require('simple-youtube-api');
const config = require('../auth.json');
const Discord = require('discord.js');
const Util = require('discord.js');
const yt_cfg = {
    key: config.yt_key,
    maxResults: config.yt_maxResults,
    part: config.yt_part,
};

const queue = new Map();
const youtube = new YouTube(yt_cfg.key);
const emojiCharacters = {
    0: '0⃣', 1: '1⃣', 2: '2⃣', 3: '3⃣', 4: '4⃣', 5: '5⃣', 6: '6⃣', 7: '7⃣', 8: '8⃣', 9: '9⃣', 10: '🔟',
};

module.exports = {
    name: 'play',
    description: 'plays a link',
    execute(message, args) {
        if (message.channel.type !== 'text')
            return;

        const { voiceChannel } = message.member;

        if (!voiceChannel) {
            return message.reply('please join a voice channel first!');
        }

        youtube.search(args.join(' '), yt_cfg.maxResults)
            .then(results => {
                if (results.length === 1) {
                    // -- Queue the first result.
                } else {
                    let embed = new Discord.RichEmbed();
                    let list_id = 1;
                    let songs = [];
                    for (const video of results) {
                        embed.addField(`${list_id}) ${video.title}`, `Uploader : ${video.raw.snippet.channelTitle}`, false);
                        list_id++;
                        songs.push(video);
                    }

                    message.channel.send(embed)
                        .then(async msg => {
                            msg.songs = songs;
                            let allowed = [];
                            for (let i = 1; i < list_id; i++) {
                                await msg.react(emojiCharacters[`${i}`]);
                                allowed.push(emojiCharacters[`${i}`]);
                            }

                            const filter = (reaction, user) => allowed.includes(reaction.emoji.name) && user.id === message.author.id;

                            msg.awaitReactions(filter, { max: 1, time: 60000, errors: ['time'] })
                                .then(async collected => {
                                    const reaction = collected.first();
                                    const index = Object.entries(emojiCharacters).find(e => e[1] === reaction._emoji.name)[0];
                                    const song = songs[index - 1];
                                    const video = await youtube.getVideoByID(song.id);
                                    msg.delete();
                                    handleVideo(video, voiceChannel);
                                })
                                .catch(error => console.error(error));
                        })
                        .catch(console.error);
                }
            }).catch(console.error);


        function handleVideo(video) {
            const serverQueue = queue.get(message.guild.id);
            const song_data = {
                id: video.id,
                title: Util.escapeMarkdown(video.title),
                url: `https://www.youtube.com/watch?v=${video.id}`,
            };

            if (!serverQueue) {
                return voiceChannel.join().then(connection => {
                    queue.set(message.guild.id, {
                        voiceChannel,
                        textChannel: message.channel,
                        connection: connection,
                        songs: [song_data],
                        volume: 5,
                    });
                    play();
                }).catch(console.error);
            }
            serverQueue.songs.push(song_data);
            serverQueue.textChannel.send(`✅ **${song_data.title}** has been added to the queue!`);
        }

        function play() {
            let serverQueue = queue.get(message.guild.id);
            if (serverQueue) {
                if (!serverQueue.connection) {
                    console.log('Not Connected');
                }
                const dispatcher = serverQueue.connection.playStream(ytdl(serverQueue.songs[0].url, {
                    quality: 'highestaudio',
                    // download part of the song before playing it
                    // helps reduces stuttering
                    highWaterMark: 1024 * 1024 * 10
                }))
                    .on('end', reason => {
                        console.log(reason);
                        if (reason === 'Stream is not generating quickly enough.')
                            console.log('Song ended.');
                        else
                            console.log(reason);
                        serverQueue.songs.shift();
                        if (serverQueue.songs.length === 0)
                            return serverQueue.voiceChannel.leave();
                        play();
                    })
                    .on('error', error => console.error(error));
                dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);

                serverQueue.textChannel.send(`🎶 Start playing: **${serverQueue.songs[0].title}**`);
            } else {
                return message.reply('There was an issue with that request!');
            }
        }

        /*
        let url = args[0];

        if (message.channel.type !== 'text')
            return;

        const { voiceChannel } = message.member;

        if (!voiceChannel) {
            return message.reply('please join a voice channel first!');
        }

        if (!ytdl.validateURL(url)) {
            return message.reply('Could not validate that youtube url');
        }

        voiceChannel.join().then(connection => {
            const stream = ytdl(url, { filter: 'audioonly', bitrate: 192000 });
            const dispatcher = connection.playStream(stream);

            dispatcher.on('end', () => voiceChannel.leave());
        });
        */
    },
};
