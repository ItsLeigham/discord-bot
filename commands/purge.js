module.exports = {

    name: 'purge',
    description: 'purge (amount) deletes x messages from channel',

    execute(message) {
        const user = message.mentions.users.first();

        const amount = parseInt(message.content.split(' ')[1]) ?
            parseInt(message.content.split(' ')[1]) :
            parseInt(message.content.split(' ')[2]);
        if (!amount)
            return message.reply('Must specify an amount to delete!').then(msg => {
                message.delete();
                msg.delete(3000);
            });
        if (!amount && !user)
            return message.reply(
                'Must specify a user and amount, or just an amount, of messages to purge!',
            ).then(msg => {
                message.delete();
                msg.delete(3000);
            });

        message.channel
            .fetchMessages({
                limit: amount,
            })
            .then(messages => {
                if (user) {
                    let filterBy = user ? user.id : message.guild.me.user.id;
                    messages = messages
                        .filter(m => m.author.id === filterBy);
                }

                let size = messages.size;
                if (size <= 0)
                    return message.reply('Can not purge any messages, this could be that the messages are over 14 days old!');
                message.channel
                    .bulkDelete(messages, true)
                    .then(() => message.delete())
                    .then(() => message.channel.send(`${messages.size} Messages Deleted!`).then(msg => msg.delete(3000)))
                    .catch((error) => console.log(error.stack));
            })
            .catch(console.error);
    },
};
