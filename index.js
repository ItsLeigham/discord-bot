const config = require('./auth.json');
const discord = require('discord.js');
const fs = require('fs');
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
const prefix = config.prefix;
const servers = require('./data/servers.json');

const client = new discord.Client();
client.commands = new discord.Collection();


for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

client.on('ready', () => {
    let save = false;

    // -- Scan all the servers the bot is currently a user of
    for (const guild of client.guilds) {
        console.log(guild[0]);

        if (!servers[guild[0]]) {
            let guild_data = guild[1];
            console.log(`Guild ${guild_data.name} does not exist!`);
            servers[guild_data.id] = { guild_name: guild_data.name };
            save = true;
        }
    }

    // -- Push that back to the file.
    if (save) {
        fs.writeFile('data/servers.json', JSON.stringify(servers), 'utf8', (err) => {
            if (err) {
                return console.error(err.stack);
            }

            console.log('Successfully saved server settings!');
        });
    }
});

client.on('message', msg => {
    if (!msg.content.startsWith(prefix) || msg.author.bot)
        return;

    const args = msg.content.slice(prefix.length).split(' ');
    const command = args.shift().toLowerCase();

    if (!client.commands.has(command))
        return;

    try {
        client.commands.get(command).execute(msg, args, client); // sending through the client, incase we need it.
    } catch (error) {
        console.error(error);
    }
});


client.login(config.discord_token);
